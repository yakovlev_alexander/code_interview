package ru.tinkoff.basic;

import java.io.*;

/**
 * Created by Alexander Yakovlev on 23/10/2017.
 */
public class QCore {
    public static void main(String[] args) throws IOException, ClassNotFoundException, CloneNotSupportedException {
        // autoboxing
//        testAutoboxing();
        // Comparing two objects
//        testObjectComparison();
//        testCaching();

//        testSerializetion();

        testClonable();
    }

    private static void testAutoboxing() {
        int i1 = 10;
        Integer i2 = new Integer(10);
        System.out.println(i2 == i1); // What result?
    }

    private static void testObjectComparison() {
        Integer i = new Integer(10);
        System.out.println(i.toString() == i.toString()); // What result?
    }

    private static void testCaching() {
        String str1 = "test";
        String str2 = "te" + "st";
        String str3 = new String("test");

        System.out.println(str1 == str2);
        System.out.println(str1 == str3);
    }

    private static void testSerializetion() throws IOException, ClassNotFoundException {
        // Проверка знания сериализации, как выводит static, зачем Serializable, serialVersionUID, transient, что вызов без конструктора

        String fileName = "serial.out";
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(fileName));
             ObjectInputStream ois = new ObjectInputStream(new FileInputStream(fileName))) {
            SerialObject object = new SerialObject(1, 2, "SerialObject");
            oos.writeObject(object);

            System.out.println(ois.readObject());

        }

    }

    static class SerialObject /*implements Serializable */ {
        private static final long serialVersionUID = 1L;

        private int version1;
        private static int version2;
        private transient String name;

        public SerialObject() {
            System.out.println("Object created");
        }

        public SerialObject(int version1, int version2, String name) {
            this.version1 = version1;
            this.version2 = version2;
            this.name = name;
        }

        @Override
        public String toString() {
            return "SerialObject{" +
                    "version1=" + version1 +
                    ", version2=" + version2 +
                    ", name='" + name + '\'' +
                    '}';
        }
    }

    private static void testClonable() throws CloneNotSupportedException {
        //Клонирование поверхностное, глубокое (2 варианта), вызов конструктора
        CloneableObject object = new CloneableObject(1, 2, 3);

        CloneableObject clone = object.clone();

        System.out.println(object == clone);

        System.out.println(clone);
    }

    static class CloneableObject implements Cloneable {
        int primitiveNum;
        Number num1;
        Number num2;

        public CloneableObject(int primitiveNum, int num1, int num2) {
            this.primitiveNum = primitiveNum;
            this.num1 = new Number(num1);
            this.num2 = new Number(num2);
        }

        @Override
        public CloneableObject clone() throws CloneNotSupportedException {
            return (CloneableObject) super.clone();
        }

        @Override
        public String toString() {
            return "CloneableObject{" +
                    "primitiveNum=" + primitiveNum +
                    ", num1=" + num1.a +
                    ", num2=" + num2.a +
                    '}';
        }
    }

    static class Number {
        int a;

        Number(int a) {
            this.a = a;
        }
    }
}
