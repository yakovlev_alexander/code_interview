  
  
__Java interview questions and tasks__
-

Topics covered:  
- Java Core (Autoboxing, Data types, Caching, Serialization, Cloning etc)
- Java Collections (incl. Maps) 
- Java Concurrency
- Java 8 Stream API
- Spring
- Hibernate

__  

_Alexander S. Yakovlev_
